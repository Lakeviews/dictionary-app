import React, { Component } from 'react';
import { Provider } from 'react-redux';

//Components
import  Dictionaries  from './components/dictionaries';
//Store
import store from './store/store';

class App extends Component {

  render() {
    console.log("rendering")
    return (
      <Provider store={store}>
        <div className="App">
          {/* <Products></Products>
          <Colors></Colors> */}
          <Dictionaries></Dictionaries>
        </div>
      </Provider>
    );
  }

}

export default App;
