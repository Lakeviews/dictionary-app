import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchColors, addColor } from '../store/actions/types';
import { KeyValue } from '../models/Dictionary';



class Colors extends Component {

    constructor(props) {
        super(props);
        this.state = {
            domain: 'Stonegrey',
            range: 'Dark Grey'
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    componentWillMount() {
        console.log('colors mounted');
        this.props.fetchColors()

    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    onSubmit(e) {
        e.preventDefault();
        this.props.addColor(new KeyValue(this.state.domain, this.state.range))

        this.props.fetchColors();
    }
    render() {
        const colors = this.props.colors.map((color, index) => (
            <tr key={index}>
                <td scope="row">{color.key}</td>
                <td>{color.value}</td>
            </tr>
        ));
        return (
            <div>
                <h1>Colors</h1>

                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="domain">Domain</label>
                        <input name="domain" type="text" maxLength="100" onChange={this.onChange} value={this.state.domain} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="range">Range</label>
                        <input name="range" type="text" maxLength="100" onChange={this.onChange} value={this.state.range} />
                    </div>
                    <input type="submit"></input>
                </form>

                <table>
                    <thead>
                        <tr>
                            <th>Domain</th>
                            <th>Range</th>
                        </tr>
                    </thead>
                    <tbody>
                        {colors}
                    </tbody>
                </table>
            </div>

        )
    }
}

const mapStateToProps = state => ({
    colors: state.colors.items
});
export default connect(mapStateToProps, { fetchColors, addColor })(Colors)




