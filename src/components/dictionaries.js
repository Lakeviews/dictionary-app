
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDictionaries, addDictionay, resetData, removeDictionary, addKeyValueToDictionary, updateKeyValueToDictionary, removeKeyValueOnDictionary, validateDictionary, resetValidations } from '../store/actions/types';
import { KeyValue } from '../models/Dictionary';

class Dictionaries extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dictionary: null,
            domainRange: new KeyValue('', ''),
            domainRangeFormMode: null,//create ot edit mode
            domainRangeIndex: null,
            dictionaryFormMode: null,
            dictionaryName: ''
        }

        this.editDictionary = this.editDictionary.bind(this);
        this.resetDataApp = this.resetDataApp.bind(this);
        this.removeDictionaryByIndex = this.removeDictionaryByIndex.bind(this);
        this.closeFormDictionary = this.closeFormDictionary.bind(this);
        this.onSubmitDictionary = this.onSubmitDictionary.bind(this);
        this.onDictionaryFormChange = this.onDictionaryFormChange.bind(this);
        this.onSubmitDomainRange = this.onSubmitDomainRange.bind(this);
        this.createDomainRange = this.createDomainRange.bind(this);
        this.editDomainRange = this.editDomainRange.bind(this);
        this.onDomainRangeFormChange = this.onDomainRangeFormChange.bind(this);
        this.resetDomainRangeForm = this.resetDomainRangeForm.bind(this);
        this.removeDomainRange = this.removeDomainRange.bind(this);
        this.validateDictionaryCheck = this.validateDictionaryCheck.bind(this);



    }
    componentWillMount() {
        this.props.fetchDictionaries();
    }

    resetDataApp(){

        this.props.resetData();
    
    }
    createDictionary() {
        console.log("create")
        this.setState({ 'dictionaryFormMode': 0 });
        this.setState({ 'dictionaryName': '' });
        this.setState({ 'dictionary': null });
    }

    editDictionary(dictionary) {
        this.props.resetValidations();
        this.setState({ 'dictionary': dictionary });
    }

    removeDictionaryByIndex(index) {

        var confirm = window.confirm("Do you want to remove this dictionary, Presss OK to proceed!");
        if (confirm === true)
            this.props.removeDictionary(index);
    }

    closeFormDictionary() {
        this.setState({ 'dictionaryFormMode': null });
        this.setState({ 'dictionaryName': '' });
    }

    onSubmitDictionary(e) {

        e.preventDefault();
        this.props.addDictionay(this.state.dictionaryName).then(result => {
            this.closeFormDictionary();
        });
    }
    onDictionaryFormChange(e) {

        this.setState({ [e.target.name]: e.target.value });

    }

    createDomainRange(e) {
        this.setState({ 'domainRangeFormMode': 0 });
        this.setState({ 'domainRange': new KeyValue('', '') });
        this.props.resetValidations();
    }

    validateDictionaryCheck(dictionary) {
        this.props.validateDictionary(dictionary);
    }

    editDomainRange(domainRange, index) {

        this.props.resetValidations();
        this.setState({ 'domainRangeFormMode': 1 });
        this.setState({ 'domainRangeIndex': index });
        this.setState({ 'domainRange': domainRange });

    }

    removeDomainRange(index) {

        var confirm = window.confirm("Do you want to remove this Domain Range, Presss OK to proceed !");
        if (confirm === true)
            this.props.removeKeyValueOnDictionary(this.state.dictionary, index);
    }

    onDomainRangeFormChange(e) {
        let domainRange = Object.assign({}, this.state.domainRange);
        domainRange[e.target.name] = e.target.value;
        this.setState({ 'domainRange': domainRange });
    }

    resetDomainRangeForm() {
        this.setState({ 'domainRangeFormMode': null });
        this.setState({ 'domainRangeIndex': null });
        this.setState({ 'domainRange': new KeyValue('', '') });
    }

    onSubmitDomainRange(e) {
        e.preventDefault();

        if (this.state.domainRangeFormMode === 0)
            this.props.addKeyValueToDictionary(this.state.dictionary, this.state.domainRange).then(result => {
                this.resetDomainRangeForm();
            });


        if (this.state.domainRangeFormMode === 1)
            this.props.updateKeyValueToDictionary(this.state.dictionary, this.state.domainRange, this.state.domainRangeIndex).then(result => {
                this.resetDomainRangeForm();
            });


    }
    render() {

        const dictionaries = this.props.dictionaries.map((dictionary, index) => (
            <tr key={index}>
                <td>{dictionary.name}</td>
                <td>{dictionary.items.length}</td>
                <td className="text-center">
                    <button type="button" onClick={() => this.editDictionary(dictionary)} className="btn btn-primary btn-sm mr-2">EDIT</button>
                    <button type="button" onClick={() => this.removeDictionaryByIndex(index)} className="btn btn-danger btn-sm">REMOVE</button>
                </td>
            </tr>
        ));

        let dictionary = null;
        let validations = [];

        if (this.state.dictionary !== null) {
            dictionary = this.state.dictionary.items.map((keyValue, index) => (
                <tr key={index}>
                    <td>{keyValue.key}</td>
                    <td>{keyValue.value}</td>
                    <td className="text-center">
                        <button type="button" onClick={() => this.editDomainRange(keyValue, index)} className="btn btn-primary btn-sm mr-2">EDIT</button>
                        <button type="button" onClick={() => this.removeDomainRange(index)} className="btn btn-danger btn-sm">REMOVE</button>
                    </td>
                </tr>
            ));

            validations = this.props.validations.map((validation, index) => (

                <div key={index}>
                    {validation.valid == null &&
                        <div className="row no-gutters">
                            <div className="col-lg-12 bg-dark text-white">{validation.msg}</div>
                        </div>
                    }

                    {validation.valid !== null &&
                        <div className="row no-gutters">
                            <div className="col-lg-9 bg-primary text-white">{validation.msg}</div>
                            {validation.valid &&
                                <div className="col-lg-3 bg-success">OK</div>
                            }
                            {validation.valid === false &&
                                <div className="col-lg-3 bg-danger">FAIL</div>
                            }
                        </div>
                    }

                </div>

            ));

        }




        return (
            <div className="container-full">

                <div className="row">
                    <div className="col-lg-7">
                        <h3>Dictionaries
                        <button type="button" onClick={() => this.createDictionary()} className="btn btn-primary btn-sm ml-2 float-right">CREATE</button>
                        <button type="button" onClick={() => this.resetDataApp()} className="btn btn-info btn-sm ml-2 float-right">RESET STORAGE</button>

                       
                        </h3>
                        {
                            this.state.dictionaryFormMode !== null &&
                            <div className="mb-5">
                                <form className="mb-5" onSubmit={this.onSubmitDictionary}>
                                    <div className="form-group">
                                        <label htmlFor="key">Enter a name for the Dictionary</label>
                                        <input name="dictionaryName" type="text" maxLength="100" className="form-control form-control-sm" onChange={this.onDictionaryFormChange} value={this.state.dictionaryName} required />
                                    </div>

                                    <small className="text-danger">{this.props.dictionarySubmitError}</small>
                                    <button type="submit" className="btn btn-primary btn-sm  float-right">SAVE</button>
                                    <button type="button" onClick={this.closeFormDictionary} className="btn btn-danger btn-sm float-right mr-2">CANCEL</button>
                                </form>
                                <hr className="mt-6" />
                            </div>

                        }

                        <table className="table table-striped table-inverse">
                            <thead className="bg-dark text-white">
                                <tr>
                                    <th>Name</th>
                                    <th>Records</th>
                                    <th className="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {dictionaries}

                            </tbody>
                        </table>
                    </div>

                    {this.state.dictionary !== null &&
                        <div className="col-lg-5">
                            <h3>Dictionary <span className="text-primary font-weight-bold">"{this.state.dictionary.name}"</span> Domains Ranges
                                    <button type="button" onClick={() => this.createDomainRange()} className="btn btn-secondary btn-sm ml-2 float-right">CREATE</button>
                                <button type="button" onClick={() => this.validateDictionaryCheck(this.state.dictionary)} className="btn btn-success btn-sm float-right">VALIDATE</button>
                            </h3>

                            {this.state.domainRangeFormMode !== null &&
                                <div className="mb-5">
                                    <form className="mb-5" onSubmit={this.onSubmitDomainRange}>
                                        <div className="form-group">
                                            <label htmlFor="key">Domain</label>
                                            <input name="key" type="text" maxLength="100" className="form-control form-control-sm" onChange={this.onDomainRangeFormChange} value={this.state.domainRange.key} required />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="value">Range</label>
                                            <input name="value" type="text" maxLength="100" className="form-control  form-control-sm" onChange={this.onDomainRangeFormChange} value={this.state.domainRange.value} required />
                                        </div>

                                        <small className="text-danger">{this.props.dictionarySubmitError}</small>
                                        <button type="submit" className="btn btn-primary btn-sm  float-right">SAVE</button>
                                        <button type="button" onClick={this.resetDomainRangeForm} className="btn btn-danger btn-sm float-right mr-2">CANCEL</button>
                                    </form>
                                    <hr className="mt-6" />
                                </div>

                            }

                            <table className="table">
                                <thead className="bg-dark text-white">
                                    <tr>
                                        <th>Domain</th>
                                        <th>Range</th>
                                        <th className="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {dictionary}
                                </tbody>
                            </table>


                            <blockquote className="blockquote">
                                {validations}
                            </blockquote>

                        </div>


                    }

                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    dictionaries: state.dictionaries.items,
    dictionarySubmitError: state.dictionaries.error,
    validations: state.dictionaries.validations
});

export default connect(mapStateToProps, { fetchDictionaries, addDictionay, resetData, removeDictionary, addKeyValueToDictionary, updateKeyValueToDictionary, removeKeyValueOnDictionary, validateDictionary, resetValidations })(Dictionaries);