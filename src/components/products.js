import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchProducts } from '../store/actions/types';


class Products extends Component {
    componentWillMount() {
        console.log('products mounted2');
        this.props.fetchProducts()
    }
    
    render() {
        const products = this.props.products.map((product, index) => (
            <tr key={index}>
                <td scope="row">{product.name}</td>
                <td>{product.color}</td>
                <td>{product.price}</td>
            </tr>
        ));
        return (
            <div>
                <h1>Products</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Color</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products}
                    </tbody>
                </table>
            </div>

        )
    }
}

const mapStateToProps = state => ({
    products: state.products.items
});
export default connect(mapStateToProps, { fetchProducts })(Products)




