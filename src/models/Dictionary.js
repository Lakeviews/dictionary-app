export class Dictionary {

    constructor(name = 'Dictionary Example', items = []) {
        this.name = name;
        this.items = items;
    }

    add(keyValue) {
        return new Promise((resolve, reject) => {

            /* VALIDATIONS */
            // let validation = this.validate(keyValue);
            // if (validation !== true)
            //     return reject(validation);

            /*  ADD THE ITEM TO THE DICTIONARY */
            this.items.push(keyValue)

            /* RESOLVE PROMISE */
            console.log("resolving")
            return resolve(keyValue);
        });
    }

    update(keyValue, index) {
        return new Promise((resolve, reject) => {

            /*  I PASSED KEYVALUE OBJECT, BUT TO INSURE THAT IS THE RIGHT RECORD A WILL RETREIVE IT USING THE INDEX */
            let keyValueToUpdate = this.items[index];
            if (keyValueToUpdate === undefined)
                return reject('No Index Found');

            /* VALIDATIONS */
            // let validation = this.validate(keyValue, index);
            // if (validation !== true)
            //     return reject(validation);


            /*  ADD THE ITEM TO THE DICTIONARY */
            this.items[index] = keyValue;

            /* RESOLVE PROMISE */
            console.log("resolving")
            return resolve(keyValue);
        });
    }

    remove(index) {
        return new Promise((resolve, reject) => {

            let keyValueToRemove = this.items[index]
            //check if index exists
            if (keyValueToRemove === undefined)
                reject('Index not found');

            //if yes remove it
            this.items.splice(index, 1);

            resolve(keyValueToRemove);


        });
    }

    validate(keyValue, index = null) {

        /* VALIDATIONS BLOCK */

        // 1- Duplicate Entry
        if (this.checkDuplicate(keyValue, index))
            return 'Duplicate Entry';

        // 2- Duplicate Entry
        if (this.forkOrDuplicateDomain(keyValue, index))
            return 'Forks or Duplicate Domains with different Ranges';

        // 3- Cycles : Two or more rows in a dictionary result in cycles,
        if (this.checkForCycles(keyValue, index))
            return 'Cycles: Two or more rows in a dictionary result in cycles, resulting in a never-ending transformation.';

        // 4- Chains : A chain structure in the dictionary (a value in Range column also appears in Domain column of
        //another entry), resulting in inconsistent transformation.
        if (this.checkForChains(keyValue, index))
            return 'Chains - chain structure detected ';

        /* END VALIDATIONS BLOCK */

        return true;

    }

    checkDuplicate(newKeyValue, index = null) {

        if (index !== null)
            return (this.items.filter((keyValue, keyValueIndex) => {
                return (index !== keyValueIndex) && (keyValue.key.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase() && keyValue.value.toLocaleLowerCase() === newKeyValue.value.toLocaleLowerCase());
            }).length > 0)

        return (this.items.filter(keyValue => {
            return keyValue.key.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase() && keyValue.value.toLocaleLowerCase() === newKeyValue.value.toLocaleLowerCase();
        }).length > 0)
    }

    forkOrDuplicateDomain(newKeyValue, index = null) {

        if (index !== null)
            return (this.items.filter((keyValue, keyValueIndex) => {
                return (index !== keyValueIndex) && (keyValue.key.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase() && keyValue.value.toLocaleLowerCase() !== newKeyValue.value.toLocaleLowerCase());
            }).length > 0)


        return (this.items.filter((keyValue, keyValueIndex) => {
            return keyValue.key.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase() && keyValue.value.toLocaleLowerCase() !== newKeyValue.value.toLocaleLowerCase();
        }).length > 0)
    }


    checkForCycles(newKeyValue, index = null) {

        if (index !== null)
            return (this.items.filter((keyValue, keyValueIndex) => {
                return (index !== keyValueIndex) && (keyValue.key.toLocaleLowerCase() === newKeyValue.value.toLocaleLowerCase() && keyValue.value.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase());
            }).length > 0)

        return (this.items.filter(keyValue => {
            return keyValue.key.toLocaleLowerCase() === newKeyValue.value.toLocaleLowerCase() && keyValue.value.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase();
        }).length > 0)
    }

    checkForChains(newKeyValue, index = null) {
        if (index !== null)
            return (this.items.filter((keyValue, keyValueIndex) => {
                return (index !== keyValueIndex) && (keyValue.key.toLocaleLowerCase() === newKeyValue.value.toLocaleLowerCase() || keyValue.value.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase());
            }).length > 0)

        return (this.items.filter(keyValue => {
            return keyValue.key.toLocaleLowerCase() === newKeyValue.value.toLocaleLowerCase() || keyValue.value.toLocaleLowerCase() === newKeyValue.key.toLocaleLowerCase();
        }).length > 0)
    }

    fetch() {
        console.log(this.items.length)
        return this.items;
    }
}

export class KeyValue {

    constructor(key = null, value = null) {
        this.key = key;
        this.value = value;
    }

}