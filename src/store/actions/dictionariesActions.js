import { Dictionary, KeyValue } from "../../models/Dictionary";

export const FETCH_DICTIONARIES = 'FETCH_DICTIONARYS';
export const ADD_DICTIONARY = 'ADD_DICTIONARY';
export const ADD_DICTIONARY_ERROR = 'ADD_DICTIONARY_ERROR';
export const REMOVE_DICTIONARY = 'REMOVE_DICTIONARY';
export const REMOVE_DICTIONARY_ERROR = 'REMOVE_DICTIONARY_ERROR';
export const ADD_KEYVALUE_TO_DICTIONARY = 'ADD_KEYVALUE_TO_DICTIONARY';
export const ADD_KEYVALUE_TO_DICTIONARY_ERROR = 'ADD_KEYVALUE_TO_DICTIONARY_ERROR';
export const UPDATE_KEYVALUE_TO_DICTIONARY = 'UPDATE_KEYVALUE_TO_DICTIONARY';
export const UPDATE_KEYVALUE_TO_DICTIONARY_ERROR = 'UPDATE_KEYVALUE_TO_DICTIONARY_ERROR';
export const REMOVE_KEYVALUE_TO_DICTIONARY = 'REMOVE_KEYVALUE_TO_DICTIONARY';
export const REMOVE_KEYVALUE_TO_DICTIONARY_ERROR = 'REMOVE_KEYVALUE_TO_DICTIONARY_ERROR';
export const KEYVALUE_VALIDATION_DICTIONARY = 'KEYVALUE_VALIDATION_DICTIONARY';
export const RESET_VALIDATION_DICTIONARY = 'RESET_VALIDATION_DICTIONARY';

var Dictionaries = [];

var exampleData = [ //Colors Dictionary
    new Dictionary('Colors', [
        new KeyValue('Stonegrey', 'Dark Grey'),
        new KeyValue('Midnight Black', 'Black'),
        new KeyValue('Mystic Silver', 'Silver')]),

    //Car Power Model D 
    new Dictionary('Cars Power', [
        new KeyValue('GTI', 'turbo injection petrol'),
        new KeyValue('TD', 'turbo diesel'),
        new KeyValue('EGTI', 'electric turbo injection'),
        new KeyValue('TDI', 'turbo diesel injectio ')])
];

export function resetData() {
    return function (dispatch) {

        //add sample data
        Dictionaries = exampleData;

        //save dictionaries in local storage
        saveDictionaries();
        dispatch({
            type: FETCH_DICTIONARIES,
            payload: Dictionaries
        });
    }
}

function localDictionary() {
    let dataLocal = localStorage.getItem('Dictionaries', JSON.stringify(Dictionaries));
    if (dataLocal === null)
        Dictionaries = exampleData;
    else 
    {
        dataLocal = JSON.parse(dataLocal)
        //normalize data to use the prototype classes created
        dataLocal.forEach(dictionary =>{
            Dictionaries.push(new Dictionary(dictionary.name, dictionary.items))
        });
    }

}

function saveDictionaries() {
    localStorage.setItem('Dictionaries', JSON.stringify(Dictionaries));
}
//load local data
localDictionary();

export function fetchDictionaries() {
    console.log("action fecth dictionaries")
    return function (dispatch) {
        dispatch({
            type: FETCH_DICTIONARIES,
            payload: Dictionaries
        });
    }
};



export function addDictionay(name = '') {
    return function (dispatch) {

        return new Promise((resolve, reject) => {
            //check if dictionary name  exists
            let dictionaryExists = (Dictionaries.filter(dictionary => {
                return dictionary.name.toLocaleLowerCase() === name.toLocaleLowerCase()
            }).length > 0);

            //if exists return and error to the store
            if (dictionaryExists) {
                dispatch({
                    type: ADD_DICTIONARY_ERROR,
                    payload: 'Dictionary Name Already Defined'
                });
                reject(false)
            } else {
                //add a new Dictionary

                let dictionary = new Dictionary(name);
                Dictionaries.push(dictionary);

                //save Dictionaries in local storage
                saveDictionaries()

                dispatch({
                    type: ADD_DICTIONARY,
                    payload: { item: dictionary, items: Dictionaries }
                });


                resolve(true)
            }

        });


    };
}

export function removeDictionary(index) {
    console.log('removeing ' + index)
    return function (dispatch) {

        return new Promise((resolve, reject) => {

            //check dictionary index exists
            let dictionary = Dictionaries[index];

            if (dictionary !== undefined) {
                //update the list
                Dictionaries.splice(index, 1);
                console.log(Dictionaries)

                 //save Dictionaries in local storage
                 saveDictionaries()

                //dispath the action REMOVE_DICTIONARY, the list was not updates so i force reload by sending it througth the payload
                dispatch({
                    type: REMOVE_DICTIONARY,
                    payload: { removedItem: dictionary, items: Dictionaries }
                });

            } else {
                //remove error due index inexistance
                dispatch({
                    type: REMOVE_DICTIONARY_ERROR,
                    payload: index
                });


                reject(true)
            }

        });


    };
}

export function addKeyValueToDictionary(dictionary: Dictionary, keyValue: KeyValue) {

   
    return function (dispatch) {

        return new Promise((resolve, reject) => {


            dictionary.add(keyValue).then(result => {

                 //save Dictionaries in local storage
                 saveDictionaries()

                dispatch({
                    type: ADD_KEYVALUE_TO_DICTIONARY,
                    payload: result
                });

                resolve(true);
            }).catch(response => {

                dispatch({
                    type: ADD_KEYVALUE_TO_DICTIONARY_ERROR,
                    payload: response
                });
                reject(false);
            });

        });
    };
}


export function updateKeyValueToDictionary(dictionary: Dictionary, keyValue: KeyValue, index: number) {

    return function (dispatch) {

        return new Promise((resolve, reject) => {

            dictionary.update(keyValue, index).then(result => {

                 //save Dictionaries in local storage
                 saveDictionaries()

                dispatch({
                    type: UPDATE_KEYVALUE_TO_DICTIONARY,
                    payload: { removedItem: result }
                });



                resolve(true);

            }).catch(response => {

                dispatch({
                    type: UPDATE_KEYVALUE_TO_DICTIONARY_ERROR,
                    payload: response
                });

                reject(false)
            });

        });

    };

}


export function removeKeyValueOnDictionary(dictionary: Dictionary, index: number) {

    return function (dispatch) {

        return new Promise((resolve, reject) => {

            dictionary.remove(index).then(response => {

                 //save Dictionaries in local storage
                 saveDictionaries()

                dispatch({
                    type: REMOVE_KEYVALUE_TO_DICTIONARY,
                    payload: response
                });

            }).catch(error => {
                dispatch({
                    type: REMOVE_KEYVALUE_TO_DICTIONARY_ERROR,
                    payload: error
                });
            })
        })
    }
}


export function validateDictionary(dictionary) {
    console.log(dictionary)
    return function (dispatch) {

        return new Promise((resolve, reject) => {

            let validationMessages = [];
            //reset validations messages
            dispatch({
                type: KEYVALUE_VALIDATION_DICTIONARY,
                payload: validationMessages
            });

            //start validating the dictionary
            dictionary.items.forEach((keyValue, index) => {

                validationMessages.push({
                    valid: null, msg: 'Validation Entry with Domain "' + keyValue.key + '" and range "' + keyValue.value + '" against dictionary'
                });

                dispatch({
                    type: KEYVALUE_VALIDATION_DICTIONARY,
                    payload: validationMessages
                });

                //validate duplicate entries for Keyvalue against dictionary entries
                if (dictionary.checkDuplicate(keyValue, index))
                    validationMessages.push({ valid: false, msg: 'Duplicate Entry Validation FAIL' });
                else
                    validationMessages.push({ valid: true, msg: 'Duplicate Entry Validation OK' });

                //Dispatch Validation for duplicate entries 
                dispatch({
                    type: KEYVALUE_VALIDATION_DICTIONARY,
                    payload: validationMessages
                });

                //validate forks or duplicates domains for Keyvalue against dictionary entries
                if (dictionary.forkOrDuplicateDomain(keyValue, index))
                    validationMessages.push({ valid: false, msg: 'Forks or Duplicate Domains with different Ranges Validation FAIL' });
                else
                    validationMessages.push({ valid: true, msg: 'Forks or Duplicate Domains with different Ranges Validation OK' });


                //Dispatch Validation for duplicate entries 
                dispatch({
                    type: KEYVALUE_VALIDATION_DICTIONARY,
                    payload: validationMessages
                });

                //validate cycles for Keyvalue against dictionary entries
                if (dictionary.checkForCycles(keyValue, index))
                    validationMessages.push({ valid: false, msg: 'Cycles Validation FAIL' });
                else
                    validationMessages.push({ valid: true, msg: 'Cycles Validation OK' });

                //Dispatch Validation for duplicate entries 
                dispatch({
                    type: KEYVALUE_VALIDATION_DICTIONARY,
                    payload: validationMessages
                });


                //validate Chains for Keyvalue against dictionary entries
                if (dictionary.checkForChains(keyValue, index))
                    validationMessages.push({ valid: false, msg: 'Chains Validation FAIL' });
                else
                    validationMessages.push({ valid: true, msg: 'Chains Validation OK' });

                //Dispatch Validation for duplicate entries 
                dispatch({
                    type: KEYVALUE_VALIDATION_DICTIONARY,
                    payload: validationMessages
                });
            });
        });
    };
}


export function resetValidations() {
    console.log("reset validations")
    return function (dispatch) {
        dispatch({
            type: RESET_VALIDATION_DICTIONARY,
            payload: []
        });
    }
};