import { Product } from '../../models/Product';
import { Dictionary, KeyValue } from '../../models/Dictionary';

// PRODUCTS
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const NEW_PRODUCT = 'NEW_PRODUCT';

// PRODUCTS FUNCTIONS
export const Products = [
    new Product("Apple iPhone 6s", "Stonegrey", 769.20),
    new Product("Samsung Galaxy S8", "Midnight Black", 569.10),
    new Product("Huawei P9", "Mystic Silver", 272.00)];

export function fetchProducts() {
    console.log("action fecth products")
    return function (dispatch) {
        dispatch({
            type: FETCH_PRODUCTS,
            payload: Products
        });
    }
};


// COLORS
export const FETCH_COLORS = 'FETCH_COLORS';
export const NEW_COLOR = 'NEW_COLOR';
export const NEW_COLOR_ADD_ERROR = 'NEW_COLOR_ADD_ERROR';

//COLORS FUNCTIONS
export const ColorsDictionary = new Dictionary([
    new KeyValue('Stonegrey', 'Dark Grey'),
    new KeyValue('Midnight Black', 'Black'),
    new KeyValue('Mystic Silver', 'Silver')
]);

export function fetchColors() {
    console.log("action fecth colors")
    return function (dispatch) {
        dispatch({
            type: FETCH_COLORS,
            payload: ColorsDictionary.fetch()
        });
    }
};

export function addColor(keyValue) {
    return function (dispatch) {
        ColorsDictionary.add(keyValue).then(result => {
        
            dispatch({
                type: NEW_COLOR,
                payload: result
            });
            dispatch({
                type: FETCH_COLORS,
                payload: ColorsDictionary.fetch()
            });

        }).catch(error => {
            console.log(error)
            dispatch({
                type: NEW_COLOR_ADD_ERROR,
                payload: error
            });
        });
    }
};


export * from './dictionariesActions';