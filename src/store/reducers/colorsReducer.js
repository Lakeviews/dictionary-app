import { FETCH_COLORS, NEW_COLOR, NEW_COLOR_ADD_ERROR } from '../actions/types';


const initialState = {
    items: [],
    item: [],
    errors: []
}

export default function (state = initialState, action) {

    switch (action.type) {
        //return the products the store
        case FETCH_COLORS:
            console.log("store action FETCH_COLORS")
            return {
                ...state,
                items: action.payload.slice()
            }

        case NEW_COLOR:
            console.log(action.payload)
            return {
                ...state,
                item: action.payload
            }

        case NEW_COLOR_ADD_ERROR:
            console.log("new color adding error ocurred")
            return {
                ...state,
                errors: action.payload
            }


        default:
            return state;
    }
}
