import {
    FETCH_DICTIONARIES,
    ADD_KEYVALUE_TO_DICTIONARY,
    ADD_KEYVALUE_TO_DICTIONARY_ERROR,
    REMOVE_KEYVALUE_TO_DICTIONARY,
    REMOVE_KEYVALUE_TO_DICTIONARY_ERROR,
    UPDATE_KEYVALUE_TO_DICTIONARY,
    UPDATE_KEYVALUE_TO_DICTIONARY_ERROR,
    KEYVALUE_VALIDATION_DICTIONARY,
    RESET_VALIDATION_DICTIONARY,
    ADD_DICTIONARY,
    ADD_DICTIONARY_ERROR,
    REMOVE_DICTIONARY,
    REMOVE_DICTIONARY_ERROR
} from '../actions/types';


const initialState = {
    items: [],
    item: [],
    subItem: null,
    error: null,
    removedItem: null,
    removedSubItem: null,
    removedIndexError: null,
    validations: []
}

export default function (state = initialState, action) {

    switch (action.type) {
        //return the products the store

        case ADD_DICTIONARY:
            console.log("store action ADD_DICTIONARY_ERROR")
            return {
                ...state,
                item: action.payload.item,
                items: action.payload.items.slice()
            }


        case ADD_DICTIONARY_ERROR:
            console.log("store action ADD_DICTIONARY_ERROR")
            return {
                ...state,
                error: action.payload
            }

        case REMOVE_DICTIONARY:
            console.log("store action REMOVE_DICTIONARY")
            console.log(action.payload.items)
            return {
                ...state,
                removedItem: action.payload.removedItem,
                items: action.payload.items.slice()
            }

        case REMOVE_DICTIONARY_ERROR:
            console.log("store action REMOVE_DICTIONARY")
            return {
                ...state,
                removedIndexError: action.payload,

            }

        case FETCH_DICTIONARIES:
            console.log("store action FETCH_DICTIONARYS")
            return {
                ...state,
                items: action.payload.slice()
            }


        case ADD_KEYVALUE_TO_DICTIONARY:
            console.log(action.payload)
            console.log("store action ADD_KEYVALUE_TO_DICTIONARY")
            return {
                ...state,
                subItem: action.payload,
                error: null

            }



        case ADD_KEYVALUE_TO_DICTIONARY_ERROR:
            console.log('ADD_KEYVALUE_TO_DICTIONARY_ERROR')
            return {
                ...state,
                error: action.payload
            }


        case UPDATE_KEYVALUE_TO_DICTIONARY:
            console.log(action.payload)
            console.log("store action UPDATE_KEYVALUE_TO_DICTIONARY")
            return {
                ...state,
                subItem: action.payload,
                error: null

            }



        case UPDATE_KEYVALUE_TO_DICTIONARY_ERROR:
            console.log('UPDATE_KEYVALUE_TO_DICTIONARY_ERROR')
            return {
                ...state,
                error: action.payload
            }


        case REMOVE_KEYVALUE_TO_DICTIONARY:
            console.log('REMOVE_KEYVALUE_TO_DICTIONARY')
            return {
                ...state,
                removedSubItem: action.payload.removedItem,
                items: state.items.slice()
            }


        case REMOVE_KEYVALUE_TO_DICTIONARY_ERROR:
            console.log('REMOVE_KEYVALUE_TO_DICTIONARY_ERROR')
            return {
                ...state,
                error: action.payload
            }


        case KEYVALUE_VALIDATION_DICTIONARY:
            console.log('KEYVALUE_VALIDATION_DICTIONARY')
            return {
                ...state,
                validations: action.payload
            }


        case RESET_VALIDATION_DICTIONARY:
            console.log('RESET_VALIDATION_DICTIONARY')
            return {
                ...state,
                validations: action.payload
            }


        default:
            return state;
    }
}
