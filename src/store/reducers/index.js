import { combineReducers } from 'redux';
import productReducer from './productReducer';
import colorsReducer from './colorsReducer';
import dictionariesReducer from './dictionariesReducer';

export default combineReducers({
    products : productReducer,
    colors: colorsReducer,
    dictionaries : dictionariesReducer
});