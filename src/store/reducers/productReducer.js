import { FETCH_PRODUCTS } from '../actions/types';


const initialState = {
    items: [],
    item: []
}

export default function (state=initialState, action) {

    switch (action.type) {
        //return the products the store
        case FETCH_PRODUCTS:

        console.log("store action FETCH_PRODUCTS")
            return {
                ...state,
                items: action.payload
            }

        default:
            return state;
    }
}
